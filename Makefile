all: execute-performance

execute-performance:
	git clone https://gitlab.com/SekarRaj/jets-performance-tests.git
	@echo "Git cloning done"
	cd jets-performance-tests
	@echo "Now building docker -jmeter- image"
	./build.sh
	@echo "Start running the container::"
	./product_load.sh