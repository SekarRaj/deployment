package gradle.cucumber

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import gradle.cucumber.entities.Product
import groovy.json.JsonSlurper
import io.cucumber.datatable.DataTable
import org.apache.http.HttpStatus
import org.apache.http.client.fluent.Request

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

@Given("Product Service is healthy")
static void prodcut_Service_is_healthy() {
    def response = Request.Get("http://product.apps.jetsdev.psteklabs.com/product/name/jam").execute().returnResponse()

    assertEquals(HttpStatus.SC_OK, response.getStatusLine().statusCode)
}

@When("Product details of {int} is searched")
void product_details_of_is_searched(Integer productId) {
    def entity = Request.Get("http://product.apps.jetsdev.psteklabs.com/product/id/${productId}")
            .execute()
            .returnResponse().getEntity().getContent().getText().toString()

//    println entity

//    println EntityUtils.toString(entity)

    resProduct = new JsonSlurper().parseText(entity)
}

@Then("Product details should be received as below")
void product_details_should_be_received_as_below(DataTable table) {
    List<Product> products = table.asList(Product.class)

    products.forEach {
        println it
        println resProduct
        assertTrue(it.category.equals(resProduct.category))
        assertTrue(it.department.equals(resProduct.department))
        assertTrue(it.name.equals(resProduct.productName))
    }
}