package gradle.cucumber.entities

import groovy.transform.ToString

@ToString
class Product {
    String id
    String name
    String category
    String department

    Product(String id, String name, String category, String department) {
        this.id = id
        this.name = name
        this.category = category
        this.department = department
    }
}
