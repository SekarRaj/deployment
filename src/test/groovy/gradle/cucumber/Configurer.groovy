package gradle.cucumber

import cucumber.api.TypeRegistry
import cucumber.api.TypeRegistryConfigurer
import gradle.cucumber.entities.Product
import io.cucumber.datatable.DataTableType
import io.cucumber.datatable.TableEntryTransformer

class Configurer implements TypeRegistryConfigurer{
    @Override
    void configureTypeRegistry(TypeRegistry registry) {

        registry.defineDataTableType(new DataTableType(Product.class, new TableEntryTransformer<Product>() {
            @Override
            Product transform(Map<String, String> entry) {
                return new Product(entry.get("id"),entry.get("name"),entry.get("category"),entry.get("department"))
            }
        }))
    }

    @Override
    Locale locale() {
        return Locale.ENGLISH;
    }
}
