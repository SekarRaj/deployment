Feature: Product Search

  Scenario: Searching for Product should give product details
    Given Product Service is healthy
     When Product details of 123 is searched
     Then Product details should be received as below
      | id  | category | department | name               |
      | 123 | Grocery  | Perishable | Spinach and Greens |
